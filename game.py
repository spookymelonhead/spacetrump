import sys
import pygame
from ship import Ship
from trump import Trump, TrumpFleet
from bullet import Bullet
from settings import Settings, COLOR, SCREEN_RESOLUTION, SHIP_RESOLUTION
from pygame.sprite import Group

class Game():
	def __init__(self):
		#settings
		self.settings=Settings(SCREEN_RESOLUTION['1200x800'],COLOR['black'])
		#level
		self.level=0
		#maneuver flags
		self.maneuver_left = False
		self.maneuver_right = False
		#Fire flag
		self.fire=False
		#screen
		self.screen = pygame.display.set_mode(self.settings.screen_resolution)
		#Object to handle multiple bullets
		self.bullets= Group()
		#Player ship
		self.falcon=Ship(self.screen, self.settings)
		#Devils 1st instance
		self.devil=Trump(self.screen, self.settings)
		#Devils fleet
		self.devilsFleet=TrumpFleet(self.settings)
		self.devilsFleet.create_fleet(self.screen, self.settings)

	def check_events(self, ship):
		"""Response to Keypresses and Mouse"""
		for event in pygame.event.get():
			#exit event
			if event.type == pygame.QUIT:
				sys.exit(0)
			#Key press event
			elif event.type == pygame.KEYUP:
				if event.key == pygame.K_RIGHT:
					self.maneuver_right = False
					print('Manuever right Key released')
				if event.key == pygame.K_LEFT:
					self.maneuver_left=False
					print('Manuever left Key released')
				if event.key == pygame.K_SPACE:
					self.fire = False 
					print('Fire Key released')
			#Key release event
			elif event.type == pygame.KEYDOWN:
				print('Key Pressed')
				if event.key == pygame.K_RIGHT:
					self.maneuver_right=True
				if event.key == pygame.K_LEFT:
					self.maneuver_left=True
				if event.key == pygame.K_SPACE:
					self.fire=True
				if event.key == pygame.K_q:
					sys.exit(0)			
			else:
				print('Undefined case')

		#Handles left and right motion of the ship
		if True == self.maneuver_right:
			ship.move_right(self.settings.ship)
		if True == self.maneuver_left:
			ship.move_left(self.settings.ship)
		#Handles firing bullets from the ship
		if True == self.fire:
			self.fire=False
			newbullet1=Bullet('bullet', self.falcon, self.settings.bullet, 'cannon1')
			newbullet2=Bullet('banana', self.falcon, self.settings.bullet, 'cannon2')
			self.bullets.add(newbullet1)
			self.bullets.add(newbullet2)

		collided_bullets=pygame.sprite.spritecollide(self.devil, self.bullets, True)
		if bool(collided_bullets):
			print("UPDATE TRUMP IMAGE")
			self.devil.trumpcry('cry', self.settings, self.screen)
		else:
			print('case2')
		collide=pygame.sprite.groupcollide(self.bullets, self.devilsFleet.trumps, True, True)
		if bool(collide):
			print('')
			#self.devilsFleet.num -= 1
		print(ship.rect.centerx)