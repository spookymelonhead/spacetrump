import pygame
from pygame.sprite import Group
from pygame.sprite import Sprite
from settings import Settings
import time

class TrumpFleet():
	def __init__(self, settings):
		self.margin = 60
		self.init_num = settings.ship.devil_fleet_initial
		self.num=0
		self.avaialble_space_x= settings.screen_resolution[0]
		#Devils fleet
		self.trumps=Group()
	def create_fleet(self, screen, settings):
		for num in range(self.init_num):
			trump=Trump(screen, settings)
			trump.update(self.num)
			self.num+=1
			self.trumps.add(trump)
	def increment_fleet(self, screen, settings):
		trump=Trump(screen, settings)
		trump.update(self.num)
		self.num+=1
		self.trumps.add(trump)
	def display_fleet(self, screen):
		for trump in self.trumps.sprites():
			trump.blitme(screen)

class Trump(Sprite):
	def __init__(self, screen, settings):
		super().__init__()
		self.trumpcry_img=pygame.image.load(settings.ship.devil_may_cry_img)
		self.trumpannoy_img=pygame.image.load(settings.ship.devil_ship_img)

		self.image=self.trumpannoy_img
		self.image_tr=pygame.transform.flip(self.image, settings.ship.devil_ship_conf[1], settings.ship.devil_ship_conf[2])
		self.image_tr=pygame.transform.scale(self.image_tr, settings.ship.devil_ship_size)
		#Any shape can be represented as rectangle
		self.rect=self.image_tr.get_rect() #ships image rect
		self.screen_rect=screen.get_rect() #screens rect

		self.trumpcry_start=0
		self.update_fleet_flag=False

		self.last_rectx=0
		self.last_recty=0
		if settings.ship.devil_ship_conf[0] == 'bottom':
			self.rect.bottom = self.screen_rect.bottom
		elif settings.ship.devil_ship_conf[0] == 'top':
			self.rect.top = self.screen_rect.top

		# Starting Position ship at the bottom center of the screen.
		self.rect.centerx = self.screen_rect.centerx

	def move_right(self, settings):
		print('incrementing ', settings.motion_step)
		# Move the ship to the right.
		self.rect.centerx += settings.motion_step
		if self.rect.centerx > 1095:
			self.rect.centerx = 1095

	def move_left(self, settings):
		print('incrementing ', settings.motion_step)
		# Move the ship to the right.
		self.rect.centerx -= settings.motion_step
		if self.rect.centerx < 95:
			self.rect.centerx = 95

	def update(self, num):
		self.image_tr=pygame.transform.scale(self.image_tr,(45,65))
		#Any shape can be represented as rectangle
		self.rect=self.image_tr.get_rect() #ships image rect
		#self.screen_rect=screen.get_rect() #screens rect
		#update position
		self.rect.centerx = self.screen_rect.centerx
		self.rect.x = 0
		self.rect.y = 0

		self.rect.x += 20 + num * 220
		self.rect.y = 20

		# Starting Position ship at the bottom center of the screen.
		print ('Updating trump ship')

	def update_imgrect(self, type, settings, screen):
		if(type == 'annoy'):
			self.trumpcry_start=0
			self.image=self.trumpannoy_img
			self.image_tr=pygame.transform.flip(self.image, settings.ship.devil_ship_conf[1], settings.ship.devil_ship_conf[2])
			self.image_tr=pygame.transform.scale(self.image_tr, settings.ship.devil_ship_size)
		elif(type == 'cry'):
			self.image=self.trumpcry_img
			self.image_tr=pygame.transform.flip(self.image, settings.ship.devil_ship_conf[1], settings.ship.devil_ship_conf[2])
			self.image_tr=pygame.transform.scale(self.image_tr, settings.ship.devil_may_cry_size)
			self.trumpcry_start=time.time()
			self.update_fleet_flag=True
			print('making trump cry')
		else:
			print('')

		#Any shape can be represented as rectangle
		self.rect=self.image_tr.get_rect() #ships image rect
		self.screen_rect=screen.get_rect() #screens rect

		self.last_rectx=0
		self.last_recty=0
		if settings.ship.devil_ship_conf[0] == 'bottom':
			self.rect.bottom = self.screen_rect.bottom
		elif settings.ship.devil_ship_conf[0] == 'top':
			self.rect.top = self.screen_rect.top

		# Starting Position ship at the bottom center of the screen.
		self.rect.centerx = self.screen_rect.centerx

	def trumpcry(self, type, settings, screen):
		self.update_imgrect(type, settings, screen)

	def blitme(self, screen):
		"""Draw ship at its current location"""
		screen.blit(self.image_tr, self.rect)