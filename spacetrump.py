import sys
import pygame
from game import Game

from ship import Ship
from trump import TrumpFleet
from pygame.sprite import Sprite

import time

def run_game():
	#Init game and create scren object
	pygame.init()
	alien=Game()
	pygame.display.set_caption("SPaceTRump")
	
	#print intro screen
	while True:
		#update screen background
		alien.screen.fill(alien.settings.bg_color)
		#check for keyboard and mouse events
		alien.check_events(alien.falcon)
		#ship1
		alien.falcon.blitme(alien.screen, alien.settings)
		#ship2
		alien.devil.blitme(alien.screen)
		alien.devilsFleet.display_fleet(alien.screen)
		#Bullets events
		alien.bullets.update(alien.settings.bullet)
		for bullet in alien.bullets.sprites():
			bullet.draw('bullet', alien.screen, alien.settings.bullet)
		for bullet in alien.bullets.copy():
			if bullet.rect.bottom < 0:
				alien.bullets.remove(bullet)
		if time.time() - alien.devil.trumpcry_start > 1:
			alien.devil.update_imgrect('annoy', alien.settings, alien.screen)
		if alien.devil.update_fleet_flag == True:
			#alien.devilsFleet.increment_fleet(alien.screen, alien.settings)
			alien.devil.update_fleet_flag=False
		pygame.display.flip()

run_game()