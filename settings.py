SCREEN_RESOLUTION_1200x800=(1200,800)
GREEN=(150,200,50)
BLACK=(0,0,0)

COLOR=[GREEN, BLACK]
SCREEN_RESOLUTION={
	'1200x800' : SCREEN_RESOLUTION_1200x800,
}
SHIP_RESOLUTION={
	'very small' : (128,128),
	'small' 	 : (256,256),
	'custom'	 : (137,197),
	'custom1'	 : (180,220),
}

COLOR={
	'green': GREEN,
	'black': BLACK,
}

class ship():
	def __init__(self):
		self.falcon_ship_img="img/sob.png"
		self.falcon_ship_size=SHIP_RESOLUTION['small']
		#(Position, Flipx, Flipy)	
		self.falcon_ship_conf=('bottom', False, False)
		
		self.devil_ship_img="img/devil.png"
		self.devil_may_cry_img="img/trumpcry.png"
		self.devil_ship_size=SHIP_RESOLUTION['custom']
		self.devil_may_cry_size=SHIP_RESOLUTION['custom1']
		#(Position, Flipx, Flipy)	
		self.devil_ship_conf=('top', False, False)
		self.devil_fleet_initial=0
		self.motion_step=3

class bullet():
	def __init__(self):
		self.speed=5
		self.width=8
		self.length=12
		self.banana_size=(40,60)
		self.color= (200,0,0)

class Settings():
	"""Class to store all game settings"""
	#default constructor
	def __init__(self):
		self.screen_resolution=SCREEN_RESOLUTION['1200x800']
		self.bg_color=COLOR['black']
		self.ship = ship()
		self.bullet = bullet()
	#parametrized contructor
	def __init__(self, size, color):
		self.screen_resolution=size
		self.bg_color=color
		self.ship = ship()
		self.bullet = bullet()