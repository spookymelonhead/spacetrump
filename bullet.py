import sys
import pygame

from settings import bullet
from pygame.sprite import Sprite

class Bullet(Sprite):
	"""Class manages behaviour of a Bullet fired from a ship"""
	def __init__(self, weapon, ship, settings, cannon):
		super().__init__()
		self.type = weapon
		if 'bullet' == weapon:
			self.rect= pygame.Rect(0, 0, settings.width, settings.length)
			self.rect.top = ship.rect.top + 100
		elif 'banana' == weapon:
			self.weapon_img=pygame.image.load('img/banana.png')
			self.weapon_img=pygame.transform.scale(self.weapon_img, settings.banana_size)
			self.weapon_img=pygame.transform.flip(self.weapon_img, False, False)
			self.rect= self.weapon_img.get_rect()
			self.rect.top = ship.rect.top + 60

		if 'cannon1' == cannon:
			self.rect.centerx = ship.rect.centerx - 31
			print('Cannon 1 Fire')
		elif 'cannon2' == cannon:
			self.rect.centerx = ship.rect.centerx + 32
			print('Cannon 2 Fire')

		self.y= float(self.rect.y)

	def update(self, settings):
		self.y= self.y - settings.speed
		self.rect.y = self.y
	
	def draw(self, weapon, screen, settings):
		if('bullet' == self.type):
			pygame.draw.rect(screen, settings.color, self.rect)
		elif ('banana' == self.type):
			screen.blit(self.weapon_img, self.rect)